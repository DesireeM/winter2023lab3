public class Application
{
	public static void main (String[] args)
	{
		Student person = new Student();
		person.determineAge = 18;
		person.saySchool = "Dawson";
		
		Student person2 = new Student();
		person2.determineAge = 21;
		person2.saySchool = "Vanier";
		
		System.out.println(person.determineAge(23));
		System.out.println(person2.determineAge(18));
		System.out.println(person.saySchool("dawson"));
		System.out.println(person2.saySchool("vanier"));
		
		Student[] section3 = new Student[3];
		section3[0] = person;
		section3[1] = person2;
		
		System.out.println(section3[0].determineAge);
		System.out.println(section3[0].saySchool);
		
		section3[2] = new Student();
		section3[2].determineAge = 20;
		section3[2].saySchool = "Brebeuf";
		
		System.out.println(section3[2].determineAge);
		System.out.println(section3[2].saySchool);
	}
	
}
